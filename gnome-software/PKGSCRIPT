# Package Maintainers
MAINTAINERS=("Evie Viau <evie@eviee.gay>")

# Package information
NAME="gnome-software"
VERSION="43.4"
_MAJOR_VERSION="43"
EPOCH=0
DESC="A software center for GNOME."
GRPS=("gnome")
URL="https://wiki.gnome.org/Apps/Software"
LICENSES=("GPL-2.0")
DEPENDS=("appstream" "appstream-glib" "cairo" "flatpak" "gdk-pixbuf2" "glib2" "glibc" "gnome-menus" "gsettings-desktop-schemas" "gtk4" "iso-codes" "json-glib" "libadwaita" "libgcc" "libgudev" "librsvg2" "libsoup3" "libostree" "pango" "polkit")
OPT_DEPENDS=()
MK_DEPENDS=("meson" "ninja" "sysprof")
PROVIDES=("gnome-software")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://download.gnome.org/sources/gnome-software/${_MAJOR_VERSION}/gnome-software-${VERSION}.tar.xz")

SUM_TYPE="sha512"
SUM=("3e4c8d29569238ffa116beb8e711f05172572b9b31de7d6f5ff1e26465c9f5be97e465f44ea2f6889e1a6c2b626a2de13b99d2e348a4ef150dca3d56c84ab5cd")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    mkdir build
    cd    build

    meson --prefix=/usr       \
          --buildtype=release \
          -Dman=false         \
          -Dpackagekit=false  \
          -Dfwupd=false       \
          -Dmalcontent=false  \
          -Dgtk_doc=false     \
          -Dtests=false       \
          -Dsoup2=false	      \
          ..

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}/build"

    ninja

    ninja test

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}/build"

    DESTDIR="${BUILD_DATA_ROOT}" ninja install

    return 0
}
