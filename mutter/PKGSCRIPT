# Package Maintainers
MAINTAINERS=("Evie Viau <evie@eviee.gay>")

# Package information
NAME="mutter"
VERSION="43.3"
_MAJOR_VERSION="43"
EPOCH=1
DESC="The window manager for GNOME. It is not invoked directly, but from GNOME Session (on a machine with a hardware accelerated video driver)."
GRPS=("gnome")
URL="https://www.gnome.org"
LICENSES=("GPL-2.0")
DEPENDS=("glibc" "libgcc" "glib2" "gnome-settings-daemon" "graphene" "libxcvt" "libxkbcommon" "pipewire" "zenity" "desktop-file-utils" "startup-notification" "libx11" "pango" "libinput" "wayland" "wayland-protocols" "xwayland" "gtk3" "xorg-server")
OPT_DEPENDS=()
MK_DEPENDS=("meson" "ninja" "sysprof" "gobject-introspection")
PROVIDES=("mutter")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://download.gnome.org/sources/mutter/${_MAJOR_VERSION}/mutter-${VERSION}.tar.xz")

SUM_TYPE="sha512"
SUM=("5e9d4b213ac674889af9c15dd52f79a28a1c38af57f08a19bd6b2bf78a799aaa2f6dcb76adb9fa8d6a94e21d988befc37a696bc06801c96e1f692c80a5aff2ae")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    mkdir build
    cd    build

    meson --prefix=/usr --buildtype=debugoptimized -Dtests=false ..

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}/build"

    ninja

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}/build"

    DESTDIR="${BUILD_DATA_ROOT}" ninja install

    return 0
}
