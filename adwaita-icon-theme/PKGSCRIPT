# Package Maintainers
MAINTAINERS=("Evie Viau <evie@eviee.gay>")

# Package information
NAME="adwaita-icon-theme"
VERSION="43"
_MAJOR_VERSION="43"
EPOCH=0
DESC="The Adwaita icon theme used by the GNOME desktop."
GRPS=("gnome")
URL="https://gitlab.gnome.org/GNOME/adwaita-icon-theme"
LICENSES=("LGPL-3.0" "CC-BY-SA-3.0")
DEPENDS=("hicolor-icon-theme")
OPT_DEPENDS=()
MK_DEPENDS=("intltool" "librsvg2" "gtk3" "shared-mime-info")
PROVIDES=("adwaita-icon-theme")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://download.gnome.org/sources/adwaita-icon-theme/${_MAJOR_VERSION}/adwaita-icon-theme-${VERSION}.tar.xz")

SUM_TYPE="sha512"
SUM=("fe0c186c2dbe87ccf2373bde1bc5ab658e8cd64bf0e5a3b9cd1117d5c1bf2ef5cc83b76b7fae54fde1566a07b572d8bb9441f437e44813338195e191dbb2a021")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    # Ensure mime types exist for the build
    update-mime-database /usr/share/mime/
    gdk-pixbuf-query-loaders --update-cache

    ./configure --prefix=/usr

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    make

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    DESTDIR="${BUILD_DATA_ROOT}" make install

    return 0
}
