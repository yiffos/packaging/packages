# Package Maintainers
MAINTAINERS=("Evie Viau <evie@eviee.gay>")

# Package information
NAME="libpeas"
VERSION="1.34.0"
_MAJOR_VERSION="1.34"
EPOCH=0
DESC="A GObject based plugins engine, and is targeted at giving every application the chance to assume its own extensibility."
GRPS=("gnome")
URL="https://wiki.gnome.org/Projects/Libpeas"
LICENSES=("LGPL-2.1")
DEPENDS=("glibc" "glib2" "gobject-introspection" "gtk3" "lua" "luajit")
OPT_DEPENDS=()
MK_DEPENDS=("meson" "ninja" "libxml2" "python-pygobject")
PROVIDES=("libpeas")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://download.gnome.org/sources/libpeas/${_MAJOR_VERSION}/libpeas-${VERSION}.tar.xz")

SUM_TYPE="sha512"
SUM=("3d8877b15d1aecdce6768bcbbd69471b8b009596e60b54a781fe961d24f18f33af62838552a98ee0aa8b2ef0855837cd10189b3b9c9040f7dc40c17f42922b1b")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    mkdir build
    cd    build

    meson --prefix=/usr --buildtype=release --wrap-mode=nofallback ..

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}/build"

    ninja
    
    if [ "$DISPLAY" != "" ] || [ "$WAYLAND_DISPLAY" != "" ]; then
        ninja test
    fi

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}/build"

    DESTDIR="${BUILD_DATA_ROOT}" ninja install

    return 0
}
